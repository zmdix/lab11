from django.db import models


# Create your models here.
class Ticket(models.Model):

    location = models.CharField(max_length=255)
    time = models.DateTimeField()

    def get_location(self):
        return self.location

    def get_time(self):
        return self.time
