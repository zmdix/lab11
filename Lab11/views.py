from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from .models import Ticket


# Create your views here.
class Root(View):

    def get(self, request):
        return render(request, 'template.html')

    def post(self, request):
        params = request.POST
        location = params['Location']
        time = params['DateTime']
        ticket = Ticket.create_ticket(time, location)

        return render(request, 'template.html')
